simavr (1.6+dfsg-3) sid; urgency=medium

  * ensure proper C string null termination (closes: #925828)
  * bump standards version up to 4.4.0

 -- Milan Kupcevic <milan@debian.org>  Wed, 28 Aug 2019 23:59:06 -0400

simavr (1.6+dfsg-2) sid; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces

  [ Piper McCorkle ]
  * Change gtkwave to suggested (Closes: #912264)

 -- Milan Kupcevic <milan@debian.org>  Mon, 11 Mar 2019 13:58:08 -0400

simavr (1.6+dfsg-1) sid; urgency=medium

  * new upstream release
  * debian/copyright: update
  * rebuild reproducible docs
  * update git tag version number removal
  * bump ABI version up, set soname to libsimavr.so.2
  * produce libsimavrparts1, a new library package
  * update simavr man page
  * fix examples tarball paths and filenames
  * correct spelling mistakes
  * bump standards version up to 4.1.4

 -- Milan Kupcevic <milan@debian.org>  Sun, 24 Jun 2018 20:37:24 -0400

simavr (1.5+dfsg1-3) sid; urgency=medium

  * debian/control: switch git repository to salsa.debian.org
  * debian/rules: improve umask variation reproducibility (closes: #883244)

 -- Milan Kupcevic <milan@debian.org>  Tue, 10 Apr 2018 23:00:43 -0400

simavr (1.5+dfsg1-2) sid; urgency=medium

  * simavr/Makefile:
     + always link to shared library
     + sort the auto generated header files content
  * debian/control: add package maintenance git repository
  * do not hide compiler flags
  * bump standards version up to 4.1.1

 -- Milan Kupcevic <milan@debian.org>  Mon, 27 Nov 2017 22:51:47 -0500

simavr (1.5+dfsg1-1) sid; urgency=medium

  * Initial release. (Closes: #568156)

 -- Milan Kupcevic <milan@debian.org>  Sat, 30 Sep 2017 23:11:30 -0400
